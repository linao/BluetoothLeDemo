package com.zxy.bluetoothledemo;

import com.clj.fastble.utils.HexUtil;
import com.socks.library.KLog;

import java.util.Calendar;
import java.util.Date;

/**
 * 描述：
 *
 * @author 无人区
 * @date 2019/3/8
 */
public class WearDeviceCMD {
    static int year;
    static int month;
    static int day;
    static int hour;
    static int minute;
    static int second;
    static {
        year   =  Calendar.getInstance().get(Calendar.YEAR) - 2000;
        month  =  Calendar.getInstance().get(Calendar.MONTH) + 1;
        day    =  Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        hour   =  Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        minute =  Calendar.getInstance().get(Calendar.MINUTE);
        second =  Calendar.getInstance().get(Calendar.SECOND);
        KLog.d("zhao",(year & 0xff) + "年" + (month & 0xff) + "月" + (day & 0xff) + "日" + (hour & 0xff) + "时" + (minute & 0xff) + "分" + (second & 0xff) + "秒" );
    }

    public static final byte[] DEVICE_NUMBER =
            new byte[]{(byte) 0x80, 0x5b, (byte) 0xba, 0x03, 0x2b, 0x4d, (byte) 0x8f, 0x39};

    public static class SendCmd{

        /**
         * 获取设备电量
         */
        public static final byte[] BATTERY_POWER =
                HexUtil.hexStringToBytes("5a0d008000000000000000000000000000000000");
//                new byte[]{0x5A, 0x0D, 0x00, (byte) 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
//                0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        /**
         * 同步参数
         */
        public static final  byte[] SYNC_PARAM =
                new byte[]{0x5A, 0x01, 0x00, 0x13, 0x03, 0x0B, 0x05, 0x05, 0x05, 0x20
                , 0x20, 0x01, 0x01, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        /**
         * 查询一天总步数
         */
        public static final byte[] SYNC_STEP_NUMBER =
                HexUtil.hexStringToBytes("5a0d008600000000000000000000000000000000");

        /**
         * 心率检测
         */
        public static final  byte[] MEASURE_HEART_RATE =
                HexUtil.hexStringToBytes("5a0d008401000100000200000000000000000000");

        /**
         * 血压检测
         */
        public static final  byte[] MEASURE_BLOOD_PRESSURE =
                HexUtil.hexStringToBytes("5a0d008b01000100000200000000000000000000");

        /**
         * 同步血氧检测
         */
        public static final  byte[] MEASURE_SPO2 =
                HexUtil.hexStringToBytes("5a0d008a01000100000200000000000000000000");

        /**
         * 同步所有睡眠时间
         */
        public static final  byte[] SYNC_SLEEP_TIME =
                HexUtil.hexStringToBytes("5a0700000000000000805bba032b4d8f39000000");

        /**
         * 同步所有血压数据
         */
        public static final  byte[] SYNC_BLOOD_PRESSURE_DATA =
                HexUtil.hexStringToBytes("5a1d000000000000000000000000000000000000");

        /**
         * 同步所有心率数据
         */
        public static final  byte[] SYNC_HEART_RATE_DATA =
                HexUtil.hexStringToBytes("5a20000000000000000000000000000000000000");

        /**
         * 同步所有血氧数据
         */
        public static final  byte[] SYNC_SPO2_DATA =
                HexUtil.hexStringToBytes("5a1b000000000000000000000000000000000000");

        /**
         * 同步计步数据
         */
        public static final  byte[] SYNC_STEP_DATA =
                HexUtil.hexStringToBytes("5a0300000000000000805bba032b4d8f39000000");
    }

}
