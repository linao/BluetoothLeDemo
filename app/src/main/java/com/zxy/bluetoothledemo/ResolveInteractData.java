package com.zxy.bluetoothledemo;

import com.clj.fastble.utils.HexUtil;
import com.socks.library.KLog;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static com.zxy.bluetoothledemo.Constants.CMD_BLOOD_PRESSURE;
import static com.zxy.bluetoothledemo.Constants.CMD_HEART_RATE;
import static com.zxy.bluetoothledemo.Constants.CMD_LENGTH_PAGE_TRANSMIT;
import static com.zxy.bluetoothledemo.Constants.CMD_SLEEP;
import static com.zxy.bluetoothledemo.Constants.CMD_SPO2;
import static com.zxy.bluetoothledemo.Constants.CMD_STEP_DATA;
import static com.zxy.bluetoothledemo.Constants.EFFECTIVE_VALUE;
import static com.zxy.bluetoothledemo.Constants.FIFTH_BYTE;
import static com.zxy.bluetoothledemo.Constants.FIRST_BYTE;
import static com.zxy.bluetoothledemo.Constants.FIRST_SN;
import static com.zxy.bluetoothledemo.Constants.FOURTH_BYTE;
import static com.zxy.bluetoothledemo.Constants.INSTRUCTION_SPO2_RECEIVE_DATA;
import static com.zxy.bluetoothledemo.Constants.LAST_SN;
import static com.zxy.bluetoothledemo.Constants.LAST_SN_OF_LENGTH_PAGE;
import static com.zxy.bluetoothledemo.Constants.REAL_VALUE;
import static com.zxy.bluetoothledemo.Constants.SECOND_BYTE;
import static com.zxy.bluetoothledemo.Constants.SIXTH_BYTE;
import static com.zxy.bluetoothledemo.Constants.TENTH_BYTE;
import static com.zxy.bluetoothledemo.Constants.THIRD_BYTE;

/**
 * 描述：处理 App 与 手环交互数据
 *
 * @author 无人区
 * @date 2019/3/11
 */
public class ResolveInteractData {


    //是否需要处理睡眠长包数据（根据APP端发送同步睡眠数据命令，返回传输包的天数及有效标记判断
    private boolean isNeedResolveSleepData = false;
    //是否需要处理血压长包数据（根据APP端发送同步睡眠数据命令，返回传输包的天数及有效标记判断
    private boolean isNeedResolveBloodPressureData = false;
    //是否需要处理血氧长包数据（根据APP端发送同步睡眠数据命令，返回传输包的天数及有效标记判断
    private boolean isNeedResolveSpo2Data = false;
    //是否需要处理心率长包数据（根据APP端发送同步睡眠数据命令，返回传输包的天数及有效标记判断
    private boolean isNeedResolveHeartRateData = false;
    //是否需要计步长包数据（根据APP端发送同步睡眠数据命令，返回传输包的天数及有效标记判断
    private boolean isNeedResolveStepData = false;

//    int stepShouldSmailPageNumber = 0;

    private static ResolveInteractData instence;
    //当前接收长包状态类型
    private int receiveLengthPageStatus;
    private FastBleActivity fastBleActivity;


    int pageLength;
    int pageIndex;
    int year;
    int month;
    int day;
    int intervalTime;
    int contentNumber;
    int sign;
    int shouldSmailPageNumber;
    byte[] firstPage;
    //存放计步数据分包  key： 分包序号 sn  value ： 分包数据
    private Map<Integer, byte[]> stepMap = new TreeMap<>();

    private ResolveInteractData() {

    }

    public static ResolveInteractData getInstence() {
        synchronized (ResolveInteractData.class) {
            if (instence == null) {
                instence = new ResolveInteractData();
            }
        }
        return instence;
    }

    /**
     * 第一层处理接收到的数据
     *
     * @param fastBleActivity
     * @param data
     */
    public void resolveInteractData(FastBleActivity fastBleActivity, byte[] data) {
        this.fastBleActivity = fastBleActivity;
        if (data[FIRST_BYTE] == 0x5a) {
            //接收到设备发出的数据
            switch (data[SECOND_BYTE]) {
                default:
                    break;
                //通用长包传输
                case CMD_LENGTH_PAGE_TRANSMIT: {
                    if (data[THIRD_BYTE] == 0) {
                        //确认包回应,接收到确认包回应后，请求下一个长包
                        KLog.d("zhao", "确认包回应  " + HexUtil.encodeHexStr(data));
                        StringBuffer sb = new StringBuffer();
                        sb.append("5a0600");
                        if (pageIndex < 10) {
                            sb.append("000" + pageIndex);
                        }
                        sb.append("000000000000000000000000000000");
                        fastBleActivity.bleWrite(HexUtil.hexStringToBytes(sb.toString()));
                    } else {
                        //普通长包中传输包
                        ResolveLengthPageData resolveLengthPageData = new ResolveLengthPageData();
                        resolveLengthPageData.resolveLengthPageData(data);
                    }
                    break;
                }
            }
        } else if (data[FIRST_BYTE] == 0x5b) {
            //接收到设备回应的数据


        }
        switch (data[SECOND_BYTE]) {
            default:
                break;
            //简单数据传输
            case Constants.CMD_EASY_DATA_TRANSMIT: {
                switch (data[FOURTH_BYTE]) {
                    default:
                        break;
                    //血压
                    case Constants.INSTRUCTION_BLOOD_PRESSURE_RECEIVE_DATA:
                        int highPressure = data[FIFTH_BYTE];
                        int lowPressure = data[SIXTH_BYTE];
                        KLog.d("zhao", "最终血压:  " + " 高压 " + highPressure + " 低压 " + lowPressure);
                        break;
                    //血氧
                    case INSTRUCTION_SPO2_RECEIVE_DATA:
                        //实时值
                        if (data[SIXTH_BYTE] == REAL_VALUE) {
                            KLog.d("zhao", "测得血氧实时值:  " + data[FIFTH_BYTE]);
                            //有效值
                        } else if (data[SIXTH_BYTE] == EFFECTIVE_VALUE) {
                            KLog.d("zhao", "测得血氧有效值:  " + data[FIFTH_BYTE]);
                        }
                        break;
                    //心率
                    case Constants.INSTRUCTION_HEART_RATE_RECEIVE_DATA:
                        //实时值
                        if (data[SIXTH_BYTE] == REAL_VALUE) {
                            KLog.d("zhao", "测得心率实时值:  " + data[FIFTH_BYTE]);
                            //有效值
                        } else if (data[SIXTH_BYTE] == EFFECTIVE_VALUE) {
                            KLog.d("zhao", "测得心率有效值:  " + data[FIFTH_BYTE]);
                        }
                        break;
                }
                break;
            }
            case CMD_SLEEP:
                if (!"0000".equals(HexUtil.encodeHexStr(new byte[]{data[THIRD_BYTE], data[FOURTH_BYTE]}))) {
                    isNeedResolveSleepData = true;
                } else {
                    isNeedResolveSleepData = false;
                }
                break;
            case CMD_HEART_RATE:
                KLog.d("zhao", "同步心率，天数:  " + data[FIFTH_BYTE]);
                if (data[FIFTH_BYTE] > 0) {
                    isNeedResolveHeartRateData = true;
                } else {
                    isNeedResolveHeartRateData = false;
                }
                break;
            case CMD_SPO2:
                KLog.d("zhao", "同步血氧，天数:  " + data[FIFTH_BYTE]);
                if (data[FIFTH_BYTE] > 0) {
                    isNeedResolveSpo2Data = true;
                } else {
                    isNeedResolveSpo2Data = false;
                }
                break;
            case CMD_BLOOD_PRESSURE:
                KLog.d("zhao", "同步血压，天数:  " + data[FIFTH_BYTE]);
                if (data[FIFTH_BYTE] > 0) {
                    isNeedResolveBloodPressureData = true;
                } else {
                    isNeedResolveBloodPressureData = false;
                }
                break;
            case CMD_STEP_DATA:
                int days = HexByteUtil.subByteToInt(data, FOURTH_BYTE, 2);
                KLog.d("zhao", "同步计步数据，天数:  " + days);
                if (days > 0) {
                    isNeedResolveStepData = true;
                } else {
                    isNeedResolveStepData = false;
                }
                break;
        }
    }

    /**
     * 处理长包
     */
    public class ResolveLengthPageData {

        /**
         * 处理计步数据
         *
         * @param data
         */
        private void resolveLengthPageData(byte[] data) {
            //长包第一个传输包
            if (data[THIRD_BYTE] == FIRST_SN) {
                KLog.d("zhao", "长包第一个传输包" + HexUtil.encodeHexStr(data));
                //1、包长度
                pageLength = HexByteUtil.subByteToInt(data, 3, 2);
                //2、包序号
                pageIndex = HexByteUtil.subByteToInt(data, 5, 2);
                //3、日期：年月日
                year = data[10];
                month = data[11];
                day = data[12];
                //4、间隔
                intervalTime = data[13];
                //5、条数
                contentNumber = data[14];
                //6、标志
                sign = data[15];
                KLog.d("zhao", " 包长度 " + pageLength + " 包序号 " + pageIndex
                        + " 年 " + year + " 月 " + month
                        + " 日 " + day + " 间隔 " + intervalTime
                        + " 条数 " + contentNumber + " 标志 " + sign);
                //7、此长包应有多少个分包
                if (pageLength % 17 > 0) {
                    shouldSmailPageNumber = pageLength / 17 + 1;
                } else {
                    shouldSmailPageNumber = pageLength / 17;
                }
                //8、当前接收长包状态类型 cmd
                receiveLengthPageStatus = data[TENTH_BYTE];
                //9、头包
                firstPage = data;
            } else {
                switch (receiveLengthPageStatus) {
                    default:
                        break;
                    case CMD_STEP_DATA:
                        //当前接收长包状态为计步数据
                        //2、以长包头包为 key，长包分包个数为 value，存到所有长包集合中
                        receiveStepOtherPage(data, pageIndex);
                        break;
                }
            }
        }

        /**
         * 接收计步数据
         *
         * @param data
         * @param pageIndex 包序号
         */
        private void receiveStepOtherPage(byte[] data, int pageIndex) {
            KLog.d("zhao", "第" + data[THIRD_BYTE] + "条分包");
            //其他分包
            //1、分包 sn 序号
            int smailPageSn = data[THIRD_BYTE];
            //2、以长包序号 + 分包 sn 序号为 key ，后边 byte[] 数据为 value存入 stepMap
            if (!stepMap.containsKey((smailPageSn & 0xff))) {
                if (data[THIRD_BYTE] != LAST_SN && data[THIRD_BYTE] != LAST_SN_OF_LENGTH_PAGE) {
                    KLog.d("zhao", "存数据");
                    //不是该长包最后一个传输包
                    stepMap.put((smailPageSn & 0xff), HexByteUtil.subByte(data, FOURTH_BYTE, data.length - FOURTH_BYTE));
                } else {
                    //1、该长包最后一个传输包
                    stepMap.put((smailPageSn & 0xff), HexByteUtil.subByte(data, FOURTH_BYTE, data.length - FOURTH_BYTE));
                    //2、已存起来的传输包的数量
                    int hasSmailPageNumber = stepMap.size();
                    //3、判断是长包的最后一个包还是传输过程的最后一个包
                    if (data[THIRD_BYTE] == LAST_SN) {
                        KLog.d("zhao", "存最后一条数据");
                    } else if (data[THIRD_BYTE] == LAST_SN_OF_LENGTH_PAGE) {
                        KLog.d("zhao", "存最最最后一条数据");
//                        getDataFromMap(stepMap);
                    }
                    //4、判断已经存起来的传输包的数量够不够应该有的传输包的数量
                    if (hasSmailPageNumber >= shouldSmailPageNumber) {
                        //1、够了，先处理这个长包的数据，处理完成后请求下一条长包
                        KLog.d("zhao", "够了，先处理这个长包的数据，处理完成后请求下一条长包");
                        resolveThisPageData(stepMap);
                    } else {
                        //不够，回应已经接收到的分包，请求重发没有收到的分包
                        KLog.d("zhao", "不够，回应已经接收到的分包，请求重发没有收到的分包");
                    }
                }
            }
        }

        /**
         * 处理当前长包的数据
         *
         * @param stepMap
         */
        private void resolveThisPageData(Map<Integer, byte[]> stepMap) {
            //存放当前长包的所有数据
            StringBuffer sb = new StringBuffer();
            for (Map.Entry<Integer, byte[]> entry : stepMap.entrySet()) {
                KLog.d("zhao", "key   " + entry.getKey() + "   value   " + HexUtil.encodeHexStr(entry.getValue()));
                sb.append(HexUtil.encodeHexStr(entry.getValue()));
            }
            KLog.d("zhao", "当前长包的所有数据" + sb.toString() + "\n数据条数  " + contentNumber);

            byte[][] newArr = HexByteUtil.splitAry(HexByteUtil.toByteArray(sb.toString()), 2);
            int steps = 0;
            for (int i = 0; i < newArr.length; i++) {
                steps += HexByteUtil.byteToInt(newArr[i]);
            }
            KLog.d("zhao", "当天总步数  " + steps);
            stepMap.clear();
            KLog.d("zhao", "够了，请求下一条长包，下一条长包包序号为 0000" + pageIndex);
            StringBuffer sb2 = new StringBuffer();
            sb2.append("5b0500");
            if (pageIndex < 10) {
                sb2.append("000" + pageIndex);
            }
            sb2.append("000000000000000000000000000000");
            fastBleActivity.bleWrite(HexUtil.hexStringToBytes(sb2.toString()));
            //2、将当前接收长包状态类型清零
            receiveLengthPageStatus = 65535;
        }

        /**
         * 从map集合中分析数据
         *
         * @param stepMap
         */
        private void getDataFromMap(Map<Integer, byte[]> stepMap) {
            for (Map.Entry<Integer, byte[]> entry : stepMap.entrySet()) {
                KLog.d("zhao", "key   " + entry.getKey() + "   value   " + HexUtil.encodeHexStr(entry.getValue()));
            }
        }
    }


    /**
     * 处理睡眠数据
     *
     * @param data
     */
    private void resolveSleepData(byte[] data) {
        //长包第一个传输包
        if (data[THIRD_BYTE] == FIRST_SN) {

        }
    }

    /**
     * 处理心率数据
     *
     * @param data
     * @param fastBleActivity
     */
    private void resolveHeartRateData(byte[] data, FastBleActivity fastBleActivity) {
        //长包第一个传输包
        if (data[THIRD_BYTE] == FIRST_SN) {

        }
    }

    /**
     * 处理血氧数据
     *
     * @param data
     */
    private void resolveSpo2Data(byte[] data) {

    }

    /**
     * 处理血压数据
     *
     * @param data
     */
    private void resolveBloodPressureData(byte[] data) {

    }
}
