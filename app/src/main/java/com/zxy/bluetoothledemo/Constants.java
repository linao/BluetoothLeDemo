package com.zxy.bluetoothledemo;

/**
 * 描述：
 *
 * @author 无人区
 * @date 2019/3/13
 */
public class Constants {

    //***********************************
    public static final byte FIRST_BYTE = 0;
    public static final byte SECOND_BYTE = 1;
    public static final byte THIRD_BYTE = 2;
    public static final byte FOURTH_BYTE = 3;
    public static final byte FIFTH_BYTE = 4;
    public static final byte SIXTH_BYTE = 5;
    public static final byte SEVENTH_BYTE = 6;
    public static final byte EIGHTH_BYTE = 7;
    public static final byte TENTH_BYTE = 9;
    //***********************************
    //代表实时值
    public static final byte REAL_VALUE = 0x00;
    //代表有效值
    public static final byte EFFECTIVE_VALUE = 0x01;
    //***********************************
    //发送传输包头
    public static final byte HEAD_SEND = 0x5a;
    //接收传输包头
    public static final byte HEAD_RECEIVE = 0x5b;
    //***********************************
    //简单数据传输 cmd
    public static final byte CMD_EASY_DATA_TRANSMIT = 0x0D;
    //通用的长包传输 cmd
    public static final byte CMD_LENGTH_PAGE_TRANSMIT = 0x05;
    //同步睡眠数据
    public static final byte CMD_SLEEP = 0x07;
    //同步心率数据
    public static final byte CMD_HEART_RATE = 0x20;
    //同步血氧数据
    public static final byte CMD_SPO2 = 0x1B;
    //同步血压数据
    public static final byte CMD_BLOOD_PRESSURE = 0x1D;
    //同步计步数据
    public static final byte CMD_STEP_DATA = 0x03;
    //***********************************
    //电池电量
    public static final byte INSTRUCTION_BATTERY = (byte) 0x80;
    //接收设备检测血压数据指令
    public static final byte INSTRUCTION_BLOOD_PRESSURE_RECEIVE_DATA = 0x06;
    //接收设备检测心率数据指令
    public static final byte INSTRUCTION_HEART_RATE_RECEIVE_DATA = 0x02;
    //接收设备检测血氧数据指令
    public static final byte INSTRUCTION_SPO2_RECEIVE_DATA = 0x05;
    //***********************************
    //长包 SN
    public static final byte FIRST_SN = 0x01;
    public static final byte SECOND_SN = 0x02;
    public static final byte THIRD_SN = 0x03;
    public static final byte FOURTH_SN = 0x04;
    public static final byte FIFTH_SN = 0x05;
    public static final byte SIXTH_SN = 0x06;
    public static final byte SEVENTH_SN = 0x07;
    public static final byte EIGHTH_SN = 0x08;
    public static final byte LAST_SN = (byte) 0xFE;
    public static final byte LAST_SN_OF_LENGTH_PAGE = (byte) 0xFF;


}
