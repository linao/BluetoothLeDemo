package com.zxy.bluetoothledemo;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clj.fastble.BleManager;
import com.clj.fastble.callback.BleGattCallback;
import com.clj.fastble.callback.BleNotifyCallback;
import com.clj.fastble.callback.BleScanCallback;
import com.clj.fastble.callback.BleWriteCallback;
import com.clj.fastble.data.BleDevice;
import com.clj.fastble.exception.BleException;
import com.clj.fastble.scan.BleScanRuleConfig;
import com.clj.fastble.utils.HexUtil;
import com.socks.library.KLog;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.runtime.Permission;

import java.util.List;


public class FastBleActivity extends AppCompatActivity implements View.OnClickListener {

    private byte[] batteryPower = WearDeviceCMD.SendCmd.BATTERY_POWER;
    private byte[] syncParam = WearDeviceCMD.SendCmd.SYNC_PARAM;
    private static final String UUID_KEY_SERVICE = "0000fee9-0000-1000-8000-00805f9b34fb";
    private static final String UUID_KEY_WRITE = "d44bc439-abfd-45a2-b575-925416129600";
    private static final String UUID_KEY_READ = "d44bc439-abfd-45a2-b575-925416129601";
    private static final String UUID_KEY_NOTIFY = "00002902-0000-1000-8000-00805f9b34fb";


    private boolean isConnected = false;
    private boolean isHasPermission = false;

    private BleScanCallbackImpl bleScanCallback;
    private BleGattCallbackImpl bleGattCallback;
    private BleNotifyCallbackImpl bleNotifyCallback;

    private BleDevice connectedBleDevice;
    private BluetoothGatt bluetoothGatt;
    private BluetoothGattService bluetoothGattService;
    private BluetoothGattCharacteristic writeCharacteristic;
    private BleWriteCallbackImpl bleWriteCallback;


    private Button btnScan;
    private Button btnSyncParam;
    /**
     * 同步步数
     */
    private Button btnSyncStepNumber;
    /**
     * 同步心率
     */
    private Button btnMeasureHeartRate;
    /**
     * 同步血压
     */
    private Button btnMeasureBloodPressure;
    /**
     * 同步血氧
     */
    private Button btnMeasureSpo2;
    /**
     * 同步睡眠时间
     */
    private Button btnSyncSleepTime;
    /**
     * 同步电池电量
     */
    private TextView tvBattery;
    /**
     * 同步步数
     */
    private TextView tvStepNumber;
    /**
     * 同步心率
     */
    private TextView tvHeartRate;
    /**
     * 同步血压
     */
    private TextView tvBloodPressure;
    /**
     * 同步血氧
     */
    private TextView tvSpo2;
    /**
     * 同步睡眠时间
     */
    private TextView tvSleepTime;
    private Button btnSyncBattery;

    byte pageLength = 0;
    byte pageIndex4 = 0;
    byte pageIndex5 = 0;
    /**
     * 同步心率
     */
    private Button btnSyncHeartRate;
    /**
     * 同步血氧
     */
    private Button btnSyncSpo2;
    /**
     * 同步血压
     */
    private Button btnSyncBloodPressure;
    /**
     * 同步计步数据
     */
    private Button btnSyncStep;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_ble);
        initPermission();
        initView();
        initParm();
        initFastBleConfig();
        new WearDeviceCMD();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BleManager.getInstance().cancelScan();
        BleManager.getInstance().destroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.btn_scan:
                if (!BleManager.getInstance().isSupportBle()) {
                    KLog.d("zhao", "蓝牙不能使用");
                    return;
                }
                if (!BleManager.getInstance().isBlueEnable()) {
                    BleManager.getInstance().enableBluetooth();
                }
                BleManager.getInstance().scan(bleScanCallback);
                break;
            //参数
            case R.id.btn_sync_param:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(syncParam);
                break;
            //电池
            case R.id.btn_sync_battery:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.BATTERY_POWER);
                break;
            //当天步数
            case R.id.btn_sync_step_number:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.SYNC_STEP_NUMBER);
                break;
            //心率
            case R.id.btn_measure_heart_rate:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.MEASURE_HEART_RATE);
                break;
            //血压
            case R.id.btn_measure_blood_pressure:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.MEASURE_BLOOD_PRESSURE);
                break;
            //血氧
            case R.id.btn_measure_spo2:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.MEASURE_SPO2);
                break;
            //睡眠时间
            case R.id.btn_sync_sleep_time:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.SYNC_SLEEP_TIME);
                break;
                //
            case R.id.btn_sync_heart_rate:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.SYNC_HEART_RATE_DATA);
                break;
            case R.id.btn_sync_spo2:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.SYNC_SPO2_DATA);
                break;
            case R.id.btn_sync_blood_pressure:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.SYNC_BLOOD_PRESSURE_DATA);
                break;
            case R.id.btn_sync_step:
                if (!isConnected) {
                    KLog.d("zhao", "未连接");
                    return;
                }
                bleWrite(WearDeviceCMD.SendCmd.SYNC_STEP_DATA);
                break;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        initPermission();
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void initPermission() {
        AndPermission.with(this)
                .runtime()
                .permission(Permission.ACCESS_FINE_LOCATION, Permission.ACCESS_COARSE_LOCATION)
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        KLog.d("zhao", "同意的权限" + data.toString());
                        isHasPermission = true;
                        initFastBleConfig();
                    }
                }).onDenied(new Action<List<String>>() {
            @Override
            public void onAction(List<String> data) {
                KLog.d("zhao", "不同意的权限" + data.toString());
                isHasPermission = false;
            }
        }).start();
    }

    private void initView() {
        btnScan = findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(this);
        btnSyncParam = findViewById(R.id.btn_sync_param);
        btnSyncParam.setOnClickListener(this);
        btnSyncBattery = (Button) findViewById(R.id.btn_sync_battery);
        btnSyncBattery.setOnClickListener(this);
        btnScan = (Button) findViewById(R.id.btn_scan);
        btnSyncParam = (Button) findViewById(R.id.btn_sync_param);
        btnSyncStepNumber = (Button) findViewById(R.id.btn_sync_step_number);
        btnSyncStepNumber.setOnClickListener(this);
        btnMeasureHeartRate = (Button) findViewById(R.id.btn_measure_heart_rate);
        btnMeasureHeartRate.setOnClickListener(this);
        btnMeasureBloodPressure = (Button) findViewById(R.id.btn_measure_blood_pressure);
        btnMeasureBloodPressure.setOnClickListener(this);
        btnMeasureSpo2 = (Button) findViewById(R.id.btn_measure_spo2);
        btnMeasureSpo2.setOnClickListener(this);
        btnSyncSleepTime = (Button) findViewById(R.id.btn_sync_sleep_time);
        btnSyncSleepTime.setOnClickListener(this);
        tvBattery = (TextView) findViewById(R.id.tv_battery);
        tvStepNumber = (TextView) findViewById(R.id.tv_step_number);
        tvHeartRate = (TextView) findViewById(R.id.tv_heart_rate);
        tvBloodPressure = (TextView) findViewById(R.id.tv_blood_pressure);
        tvSpo2 = (TextView) findViewById(R.id.tv_spo2);
        tvSleepTime = (TextView) findViewById(R.id.tv_sleep_time);
        btnSyncHeartRate = (Button) findViewById(R.id.btn_sync_heart_rate);
        btnSyncHeartRate.setOnClickListener(this);
        btnSyncSpo2 = (Button) findViewById(R.id.btn_sync_spo2);
        btnSyncSpo2.setOnClickListener(this);
        btnSyncBloodPressure = (Button) findViewById(R.id.btn_sync_blood_pressure);
        btnSyncBloodPressure.setOnClickListener(this);
        btnSyncStep = (Button) findViewById(R.id.btn_sync_step);
        btnSyncStep.setOnClickListener(this);
    }

    private void initParm() {
        bleScanCallback = new BleScanCallbackImpl();
        bleGattCallback = new BleGattCallbackImpl();
        bleNotifyCallback = new BleNotifyCallbackImpl();
        bleWriteCallback = new BleWriteCallbackImpl();
    }

    /**
     * 初始化FastBle全局配置
     */
    private void initFastBleConfig() {
        if (!isHasPermission) {
            KLog.d("zhao", "没有权限");
            return;
        }
        BleManager.getInstance().init(getApplication());
        BleManager.getInstance()
                .enableLog(true)
                .setConnectOverTime(20000)
                .setReConnectCount(1, 5000)
                .setSplitWriteNum(20)
                .setOperateTimeout(5000);
        initScanRule();
    }

    /**
     * 初始化扫描规则
     */
    private void initScanRule() {
        BleScanRuleConfig bleScanRuleConfig = new BleScanRuleConfig.Builder()
                .setDeviceName(true, "W030L-FPQS")
                .setScanTimeOut(10000)
                .setAutoConnect(true)
                .build();
        BleManager.getInstance().initScanRule(bleScanRuleConfig);
    }


    /**
     * 开始接收通知
     */
    private void startReceiveNotify(BleDevice bleDevice) {
        BleManager.getInstance().notify(bleDevice,
                UUID_KEY_SERVICE,
                UUID_KEY_READ,
                false,
                bleNotifyCallback);
    }

    /**
     * 写
     */
    public void bleWrite(byte[] writeData) {
        BleManager.getInstance()
                .write(connectedBleDevice,
                        UUID_KEY_SERVICE,
                        UUID_KEY_WRITE,
                        writeData,
                        bleWriteCallback);
    }

    /**
     * 扫描蓝牙设备回调类
     */
    class BleScanCallbackImpl extends BleScanCallback {

        @Override
        public void onScanFinished(List<BleDevice> scanResultList) {
            KLog.d("zhao", " 扫描完成 " + scanResultList.toString());
        }

        @Override
        public void onScanStarted(boolean success) {
            KLog.d("zhao", "开始扫描" + success);
        }

        @Override
        public void onScanning(BleDevice bleDevice) {
            KLog.d("zhao", "扫描到一个符合扫描规则的BLE设备（主线程）" + bleDevice.getKey());
            BleManager.getInstance().cancelScan();
            BleManager.getInstance().connect(bleDevice, bleGattCallback);
        }
    }

    /**
     * 连接蓝牙设备回调类
     */
    class BleGattCallbackImpl extends BleGattCallback {

        @Override
        public void onStartConnect() {
            KLog.d("zhao", "开始连接");
        }

        @Override
        public void onConnectFail(BleDevice bleDevice, BleException exception) {
            KLog.d("zhao", "连接失败，异常信息 " + exception.getDescription() + exception.getCode());
            switch (exception.getCode()) {
                default:
                    break;
                case BleException.ERROR_CODE_TIMEOUT:
                    BleManager.getInstance().connect(bleDevice, bleGattCallback);
                    break;
            }
        }

        @Override
        public void onConnectSuccess(BleDevice bleDevice, BluetoothGatt gatt, int status) {
            KLog.d("zhao", "连接成功  " + status);
            setConnected(true);
            setConnectedBleDevice(bleDevice);
            setBluetoothGatt(gatt);
            startReceiveNotify(bleDevice);
        }

        @Override
        public void onDisConnected(boolean isActiveDisConnected, BleDevice device, BluetoothGatt gatt, int status) {
            setConnected(false);
            if (isActiveDisConnected) {
                KLog.d("zhao", "主动调用方法断开连接  " + status);
            } else {
                KLog.d("zhao", "被动断开连接  " + status);
            }
        }
    }

    /**
     * 通知消息
     */
    class BleNotifyCallbackImpl extends BleNotifyCallback {

        @Override
        public void onNotifySuccess() {
            KLog.d("zhao", "接收通知成功");
            setConnected(true);
        }

        @Override
        public void onNotifyFailure(BleException exception) {
            KLog.d("zhao", "接收通知异常,异常信息 " + exception.getDescription());
        }

        @Override
        public void onCharacteristicChanged(byte[] data) {
            if (data.length >= 20){
                KLog.d("qian", "接收通知成功，数据    "
                        + HexUtil.encodeHexStr(HexByteUtil.subByte(data, 0, 10)) + "  " +
                        HexUtil.encodeHexStr(HexByteUtil.subByte(data, 9, 10)));
            }else {
                KLog.d("qian", "接收通知成功，数据    " + HexUtil.encodeHexStr(data));
            }
            ResolveInteractData.getInstence().resolveInteractData(FastBleActivity.this, data);
        }
    }

    /**
     * 发送数据回调类
     */
    class BleWriteCallbackImpl extends BleWriteCallback {
        /**
         * 写入成功
         *
         * @param current   表示当前发送第几包数据
         * @param total     表示本次总共多少包数据
         * @param justWrite 表示刚刚发送成功的数据包
         */
        @Override
        public void onWriteSuccess(int current, int total, byte[] justWrite) {
            KLog.d("zhao", "写入成功 " + current + "  " + total + "                 " + HexUtil.encodeHexStr(justWrite));
        }

        @Override
        public void onWriteFailure(BleException exception) {
            KLog.d("zhao", "写入失败，异常信息  " + exception.getDescription());
        }
    }

    public BluetoothGatt getBluetoothGatt() {
        return bluetoothGatt;
    }

    public void setBluetoothGatt(BluetoothGatt bluetoothGatt) {
        this.bluetoothGatt = bluetoothGatt;
    }

    public BluetoothGattService getBluetoothGattService() {
        return bluetoothGattService;
    }

    public void setBluetoothGattService(BluetoothGattService bluetoothGattService) {
        this.bluetoothGattService = bluetoothGattService;
    }

    public BluetoothGattCharacteristic getWriteCharacteristic() {
        return writeCharacteristic;
    }

    public void setWriteCharacteristic(BluetoothGattCharacteristic writeCharacteristic) {
        this.writeCharacteristic = writeCharacteristic;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }


    public BleDevice getConnectedBleDevice() {
        return connectedBleDevice;
    }

    public void setConnectedBleDevice(BleDevice connectedBleDevice) {
        this.connectedBleDevice = connectedBleDevice;
    }

    public void setTvBattery(String data) {
        tvBattery.setText(data);
    }

    public void setTvStepNumber(String data) {
        tvStepNumber.setText(data);
    }
}
