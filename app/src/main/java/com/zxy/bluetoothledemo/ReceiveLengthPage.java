package com.zxy.bluetoothledemo;

/**
 * 描述：
 *
 * @author 无人区
 * @date 2019/3/15
 */
public class ReceiveLengthPage implements IReceiverLengthPage{

    @Override
    public void start() {
        //开始接收某一种长包数据，将接收长包状态设置为该类型
    }

    @Override
    public void receivedFirstPage() {
        //如果传输包序号 SN 为 01 ，则接收到的传输包为每个长包的第一条传输包
    }

    @Override
    public void receivedOtherPage() {
        //如果 01 < 接收到的传输包的序号 < 0xfe  ，则接收到的传输包为数据包
    }

    @Override
    public void receivedLastPage() {
        //如果接收到的传输包的序号为 0xfe  ，则接收到的传输包为长包中最后一条传输包
    }

    @Override
    public void receivedAllPage() {
        //接收完成长包中所有的传输包
    }

    @Override
    public void resolveLengthPage() {
        //处理本长包数据，发送确认包
    }

    @Override
    public void requestNextLengthPage() {
        //请求下一个长包数据
    }

    @Override
    public void stop() {
        //开始接收本类型长包数据，将接收长包状态清零
    }
}
