package com.zxy.bluetoothledemo;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.socks.library.KLog;
import com.yanzhenjie.permission.Action;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.runtime.Permission;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String UUID_KEY_SERVICE = "0000fee9-0000-1000-8000-00805f9b34fb";
    private static final String UUID_KEY_WRITE = "d44bc439-abfd-45a2-b575-925416129600";
    private static final String UUID_KEY_READ = "d44bc439-abfd-45a2-b575-925416129601";
    private static final String UUID_KEY_BATTERY_SERVICE = "0000180f-0000-1000-8000-00805f9b34fb";
    private static final String UUID_KEY_BATTERY_READ = "00002a19-0000-1000-8000-00805f9b34fb";
    private static final String UUID_KEY_HID_SERVICE = "00001812-0000-1000-8000-00805f9b34fb";
    private static final String UUID_KEY_HID_READ = "00002a4a-0000-1000-8000-00805f9b34fb";
    private static final String UUID_KEY_NOTIFY = "00002902-0000-1000-8000-00805f9b34fb";

    /**
     * 开始扫描
     */
    private Button btnScan;
    private BluetoothAdapter bluetoothAdapter;
    /**
     * 停止扫描
     */
    private Button btnStopScan;
    /**
     * 断开连接
     */
    private Button btnDisconnect;
    /**
     * 连接
     */
    private Button btnConnect;
    private ScanCallbackImpl scanCallback;
    private BluetoothLeScanner bluetoothLeScanner;
    private static final int STOP_SCAN_BLUETOOTH = 1;
    private static final int STOP_SCAN_TIME = 12000;
    List<String> deviceAddressList = new ArrayList<>();
    List<ScanResult> scanResultList = new ArrayList<>();
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                default:
                    break;
                case STOP_SCAN_BLUETOOTH:
                    stopScan();
                    break;
            }
        }
    };
    private BluetoothGatt bluetoothGatt;
    private Button btnSendData;
    private Button btnReadData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initPermission();
        initParm();
        initView();
    }

    private void initPermission() {
        AndPermission.with(this)
                .runtime()
                .permission(Permission.ACCESS_COARSE_LOCATION, Permission.ACCESS_FINE_LOCATION)
                .onGranted(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        KLog.d("zhao", "同意的哪些权限" + data.toString());
                    }
                })
                .onDenied(new Action<List<String>>() {
                    @Override
                    public void onAction(List<String> data) {
                        KLog.d("zhao", "没同意的哪些权限" + data.toString());
                    }
                }).start();
    }

    private void initParm() {
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        scanCallback = new ScanCallbackImpl();
    }

    private void initView() {
        btnScan = (Button) findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(this);
        btnStopScan = (Button) findViewById(R.id.btn_stop_scan);
        btnStopScan.setOnClickListener(this);
        btnDisconnect = (Button) findViewById(R.id.btn_disconnect);
        btnDisconnect.setOnClickListener(this);
        btnConnect = (Button) findViewById(R.id.btn_connect);
        btnConnect.setOnClickListener(this);
        btnSendData = (Button) findViewById(R.id.btn_send_data);
        btnSendData.setOnClickListener(this);
        btnReadData = (Button) findViewById(R.id.btn_read_data);
        btnReadData.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
            case R.id.btn_scan:
                scan();
                break;
            case R.id.btn_stop_scan:
                stopScan();
                break;
            case R.id.btn_connect:
                bluetoothGatt.connect();
                KLog.d("zhao", "连接");
                break;
            case R.id.btn_disconnect:
                bluetoothGatt.disconnect();
                KLog.d("zhao", "断开连接");
                break;
            case R.id.btn_send_data:
                //                                   年    月    日    时    分   秒
                write(new byte[]{0x5A, 0x01, 0x00, 0x05, 0x05, 0x05, 0x05, 0x05, 0x05, 0x20
                        , 0x20, 0x01, 0x01, 0x78, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
//                write(new byte[]{0x5A, 0x10, 0x00});
                KLog.d("zhao", "发送数据");
                break;
            case R.id.btn_read_data:
                BluetoothGattService service = bluetoothGatt.getService(UUID.fromString(UUID_KEY_HID_SERVICE));
                BluetoothGattCharacteristic readCharacteristic = service.getCharacteristic(UUID.fromString(UUID_KEY_HID_READ));
                bluetoothGatt.readCharacteristic(readCharacteristic);
                KLog.d("zhao", "读取数据");
                break;
        }
    }

    private void stopScan() {
        if (bluetoothLeScanner == null && !bluetoothAdapter.isDiscovering()) {
            return;
        }
        deviceAddressList.clear();
        bluetoothLeScanner.stopScan(scanCallback);
        KLog.d("zhao", "停止扫描");
    }

    private void scan() {
        KLog.d("zhao", "开始扫描");
        List<ScanFilter> scanFilterList = new ArrayList<>();
        ScanFilter scanFilter = new ScanFilter.Builder().setDeviceName("W030L-FPQS").build();
        scanFilterList.add(scanFilter);
//        ScanFilter scanFilter2 = new ScanFilter.Builder().setDeviceName("Nordic_UART").build();
//        scanFilterList.add(scanFilter2);
        bluetoothLeScanner.startScan(scanFilterList, new ScanSettings.Builder().build(), scanCallback);
        handler.sendEmptyMessageDelayed(STOP_SCAN_BLUETOOTH, STOP_SCAN_TIME);
    }

    public class ScanCallbackImpl extends ScanCallback {

        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            String deviceAddress = result.getDevice().getAddress();
            if (!deviceAddressList.contains(deviceAddress)) {
                scanResultList.add(result);
                deviceAddressList.add(deviceAddress);
                stopScan();
            } else {
                return;
            }
            for (ScanResult scanResult : scanResultList) {
                KLog.d("zhao", " 扫描结果  " + scanResult.toString() + " 设备名 " + scanResult.getDevice().getName());
                bluetoothGatt = scanResult.getDevice().connectGatt(MainActivity.this, true, new BluetoothGattCallbackImpl());
            }

        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            KLog.d("zhao", "  一批扫描结果  " + results);
        }

        @Override
        public void onScanFailed(int errorCode) {
            KLog.d("zhao", "  扫描失败，错误原因  " + errorCode);
        }
    }

    public class BluetoothGattCallbackImpl extends BluetoothGattCallback {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            KLog.d("zhao", "  gatt  " + gatt.getDevice().getAddress() + "  status  " + status + "  newState  " + newState);
            switch (newState) {
                default:
                    break;
                case BluetoothProfile.STATE_CONNECTED:
                    KLog.d("zhao", "已经连接");
                    KLog.d("zhao", "发现服务" + gatt.discoverServices());
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    KLog.d("zhao", "连接断开");
                    break;
                case BluetoothProfile.STATE_DISCONNECTING:
                    KLog.d("zhao", "正在连接");
                    break;
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                KLog.d("zhao", "服务个数" + gatt.getServices().size());
                int i = 0;
                for (BluetoothGattService service : gatt.getServices()) {
                    KLog.d("zhao", "第 " + ++i + " 个服务的UUID    " + service.getUuid().toString());
                    KLog.d("zhao", "第 " + i + " 个服务特性个数 " + service.getCharacteristics().size());
                    int j = 0;
                    for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                        KLog.d("zhao", "第 " + i + " 个服务的第 " + ++j + " 特性的UUID    " + characteristic.getUuid().toString());
                        KLog.d("zhao", "第 " + i + " 个服务的第 " + j + " 特性的类型    " + characteristic.getWriteType());
                        int k = 0;
                        for (BluetoothGattDescriptor bluetoothGattDescriptor : characteristic.getDescriptors()) {
                            KLog.d("zhao", "第 " + i + " 个服务的第 " + j + " 特性的类型的第 " + ++k + " 个描述的UUID " + bluetoothGattDescriptor.getUuid().toString());
                        }
                    }
                }

                BluetoothGattService service = gatt.getService(UUID.fromString(UUID_KEY_SERVICE));
                BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(UUID_KEY_READ));
                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString(UUID_KEY_NOTIFY));
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                bluetoothGatt.writeDescriptor(descriptor);
                bluetoothGatt.setCharacteristicNotification(characteristic, true);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            KLog.d("zhao", "数据发生改变: " + toHexString(characteristic.getValue()));
        }

        @Override
        public void onCharacteristicRead(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
            KLog.d("zhao", "读取到数据" + characteristic.toString() + "  状态  " + status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                KLog.d("zhao", "读取到数据: " + toHexString(characteristic.getValue()));
            }
        }

        @Override
        public void onCharacteristicWrite(final BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic, final int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                KLog.d("zhao", "写入数据:  " + toHexString(characteristic.getValue()));
            }
        }
    }

    public void write(byte[] data) {   //一般都是传byte
        BluetoothGattCharacteristic writeCharacteristic = getCharcteristic(UUID_KEY_SERVICE, UUID_KEY_WRITE);  //这个UUID都是根据协议号的UUID
        if (writeCharacteristic == null) {
            KLog.e("zhao", "Write failed. GattCharacteristic is null.");
            return;
        }
        writeCharacteristic.setValue(data); //为characteristic赋值
        writeCharacteristicWrite(writeCharacteristic);
    }

    public void writeCharacteristicWrite(BluetoothGattCharacteristic characteristic) {
        if (bluetoothAdapter == null || bluetoothGatt == null) {
            KLog.e("zhao", "BluetoothAdapter not initialized");
            return;
        }
        KLog.e("zhao", "BluetoothAdapter 写入数据");
        boolean isBoolean = false;
        isBoolean = bluetoothGatt.writeCharacteristic(characteristic);
        KLog.e("zhao", "BluetoothAdapter_writeCharacteristic = " + isBoolean);  //如果isBoolean返回的是true则写入成功
    }

    private BluetoothGattCharacteristic getCharcteristic(String serviceUUID, String characteristicUUID) {

        //得到服务对象
        BluetoothGattService service = getService(UUID.fromString(serviceUUID));

        if (service == null) {
            KLog.e("zhao", "Can not find 'BluetoothGattService'");
            return null;
        }

        //得到此服务结点下Characteristic对象
        final BluetoothGattCharacteristic gattCharacteristic = service.getCharacteristic(UUID.fromString(characteristicUUID));
        if (gattCharacteristic != null) {
            return gattCharacteristic;
        } else {
            KLog.e("zhao", "Can not find 'BluetoothGattCharacteristic'");
            return null;
        }
    }

    public BluetoothGattService getService(UUID uuid) {
        if (bluetoothAdapter == null || bluetoothGatt == null) {
            KLog.e("zhao", "BluetoothAdapter not initialized");
            return null;
        }
        return bluetoothGatt.getService(uuid);
    }

    /**
     * 字节数组转成16进制表示格式的字符串
     *
     * @param byteArray 需要转换的字节数组
     * @return 16进制表示格式的字符串
     **/
    public static String toHexString(byte[] byteArray) {
        if (byteArray == null || byteArray.length < 1) {
            throw new IllegalArgumentException("this byteArray must not be null or empty");
        }

        final StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < byteArray.length; i++) {
            //0~F前面不零
            if ((byteArray[i] & 0xff) < 0x10) {
                hexString.append("0");
            }
            hexString.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        return hexString.toString().toLowerCase();
    }
}
