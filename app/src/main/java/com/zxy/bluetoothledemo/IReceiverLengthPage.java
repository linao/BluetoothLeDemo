package com.zxy.bluetoothledemo;

/**
 * 描述：接收长包流程
 *
 * @author 无人区
 * @date 2019/3/15
 */
public interface IReceiverLengthPage {

    /**
     * 开始接收长包
     */
    void start();

    /**
     * 接收到第一条传输包
     */
    void receivedFirstPage();

    /**
     * 接收到其他传输包
     */
    void receivedOtherPage();

    /**
     * 接收到本长包最后一条传输包
     */
    void receivedLastPage();

    /**
     * 接收完本长包中所有传输包
     */
    void receivedAllPage();

    /**
     * 处理接收到的长包
     */
    void resolveLengthPage();

    /**
     * 请求发送方发送下一条长包
     */
    void requestNextLengthPage();

    /**
     * 停止接收长包
     */
    void stop();
}
